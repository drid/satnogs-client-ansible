#!/bin/sh -e
#
# Raspberry PI VideoCore sensors script
#
# Copyright (C) 2019 Libre Space Foundation <https://libre.space/>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

VCGENCMD_CMD="vcgencmd"

usage() {
	cat <<EOF
Usage: $(basename "$0") [OPTIONS]... [COMMANDS]...
Raspberry PI VideoCore sensors script.

COMMANDS:
  temp                  Show core temperature
  volts [core|sdram_c|sdram_i|sdram_p]
                        Show core, SDRAM controller, I/O or physical
                         voltage
  clock [arm|core]      Show ARM or core clock
  codec [h264|mpg2|wvc1|mpg4|mjpg|wmv9]
                        Show if H264, MPG2, WVC1, MPG4, MJPG or WMV9
                         codec is enabled

OPTIONS:
  --help                Print usage
EOF
	exit 1
}

parse_args() {
	while [ $# -gt 0 ]; do
		arg="$1"
		case $arg in
			temp)
				command="measure_temp"
				awk='BEGIN { FS="=" } /^temp=/ { sub("'\''C$",""); print $2 }'
				break
				;;
			volts)
				shift
				subarg="$1"
				case $subarg in
					core|sdram_c|sdram_i|sdram_p)
						command="measure_volts $subarg"
						awk='BEGIN { FS="=" } /^volt=/ { sub("V$",""); print $2 }'
						break
						;;
					*)
						usage
						exit 1
						;;
				esac
				break
				;;
			clock)
				shift
				subarg="$1"
				case $subarg in
					arm|core)
						command="measure_clock $subarg"
						awk='BEGIN { FS="=" } /^frequency\([0-9]+\)=/ { print $2 }'
						break
						;;
					*)
						usage
						exit 1
						;;
				esac
				break
				;;
			codec)
				shift
				subarg="$1"
				case $subarg in
					h264|mpg2|wvc1|mpg4|mjpg|wmv9)
						command="codec_enabled $subarg"
						awk='BEGIN { FS="=" } /^'"$subarg"'=/ { print $2 }'
						break
						;;
					*)
						usage
						exit 1
						;;
				esac
				break
				;;
			*)
				usage
				exit 1
				;;
		esac
		shift
	done
}

main() {
	parse_args "$@"
	if [ -z "$command" ]; then
		usage
		exit 1
	fi
	$VCGENCMD_CMD "$command" | awk "$awk"
}

main "$@"
